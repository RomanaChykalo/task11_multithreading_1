package com.chykalo.view;
@FunctionalInterface
public  interface Printable{
    void print();
}