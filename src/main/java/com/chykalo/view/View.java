package com.chykalo.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import com.chykalo.controller.FibonacciNumber;
import com.chykalo.controller.Process;
import com.chykalo.controller.SleepTime;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(View.class);

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Print sum of Fibonacci numbers");
        menu.put("2", "2 - Test ping-pong game");
        menu.put("3", "3 - Test scheduled thread execution");
        menu.put("Q", "Q - Exit;");
    }

    public View() {
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", FibonacciNumber::run);
        methodsMenu.put("2", Process::testPingPong);
        methodsMenu.put("3", SleepTime::testScheduledExecution);

    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                logger.info(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}