package com.chykalo.consts;

public class InfoMessage {
    public static final String EMPTY_SPACE = " ";
    public static final String CURRENT_INFO = "Current score => ";
    public static final String BYE_MESSAGE = "GoodBye! See you at another invocation!";
    public static final String ENTER_NUMBER_INFO = "Please, enter a number, and you will see sum of Fibonacci sequence";
}
