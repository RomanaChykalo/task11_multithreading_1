package com.chykalo.consts;

public class ExceptionMessage {
    public static final String GENERAL_EXCEPTION = "Some trouble was happened";
    public static final String INTERRUPTED_EXCEPTION = "The thread is asked to complete its work";
}
