package com.chykalo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.*;

import static com.chykalo.consts.ExceptionMessage.INTERRUPTED_EXCEPTION;
import static com.chykalo.consts.InfoMessage.BYE_MESSAGE;
import static com.chykalo.consts.InfoMessage.EMPTY_SPACE;

class MyThread implements Runnable {
    private String threadName;

    public MyThread(String name) {
        this.threadName = name;
    }

    @Override
    public void run() {
        System.out.println(System.currentTimeMillis() + EMPTY_SPACE + Thread.currentThread().getName());
    }
}

public class SleepTime {
    private static final Logger logger = LogManager.getLogger(SleepTime.class);
    public static void testScheduledExecution() {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
        Runnable delayedTask = () -> logger.info(System.currentTimeMillis() + EMPTY_SPACE + Thread.currentThread().getName());
        Callable callableDelayedTask = () -> {
            return BYE_MESSAGE;
        };
        executorService.scheduleWithFixedDelay(delayedTask, 2, 2, TimeUnit.SECONDS);
        ScheduledFuture future = executorService.schedule(callableDelayedTask, 10, TimeUnit.SECONDS);
        try {
            logger.info("Callable returned " + future.get());
        } catch (InterruptedException e) {
           logger.error(INTERRUPTED_EXCEPTION);
        } catch (ExecutionException e) {
            logger.error(e.getCause());
        }
        executorService.shutdown();
    }

    public static void executeTaskPeriodically() {
        ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(8);
        Thread thread = new Thread(new MyThread("Task 1"));
        System.out.println("Current time " + System.currentTimeMillis());
        ScheduledFuture futureResult = scheduledExecutor.scheduleAtFixedRate(thread, 2, 2, TimeUnit.SECONDS);
        try {
            TimeUnit.SECONDS.sleep(50);
        } catch (InterruptedException e) {
            logger.error(INTERRUPTED_EXCEPTION);
        }
        scheduledExecutor.shutdown();
        logger.info("Executor is shut down " + scheduledExecutor.isShutdown());

    }
}
