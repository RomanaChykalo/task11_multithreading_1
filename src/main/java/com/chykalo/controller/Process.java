package com.chykalo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Process extends Thread {
    private static final Logger logger = LogManager.getLogger(Process.class);
    private static int startPoint = 0;
    private static Object sync = new Object();

    public static void testPingPong() {
        new Thread(() -> {
            for (int i = 0; i < 4; i++) {
                synchronized (sync) {
                    logger.info("PING");
                    logger.info(startPoint++);
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();

        new Thread(
                () -> {
                    for (int i = 0; i < 2; i++) {
                        synchronized (sync) {
                            logger.info("PONG");
                            sync.notify();
                            logger.info(startPoint++);
                        }
                    }
                }
        ).start();
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
