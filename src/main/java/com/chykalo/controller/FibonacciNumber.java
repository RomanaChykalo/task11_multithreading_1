package com.chykalo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.chykalo.consts.ExceptionMessage.GENERAL_EXCEPTION;
import static com.chykalo.consts.ExceptionMessage.INTERRUPTED_EXCEPTION;
import static com.chykalo.consts.InfoMessage.ENTER_NUMBER_INFO;

public class FibonacciNumber {
    private static final Logger logger = LogManager.getLogger(FibonacciNumber.class);
    private static int findFibonacciNumber(int orderNumber){
        if (orderNumber<=2){
            return 1;
        } else {
            return findFibonacciNumber(orderNumber-1)+findFibonacciNumber(orderNumber-2);
        }
    }
    private static Callable<Integer> getSum(int maxNumber){
      return ()->{int sum=2;
            for(int i=3;i<=maxNumber;i++){
              sum+=findFibonacciNumber(i);
            }
            return sum;
        };
    }
    public void printSumOfFibonacciNumber(int maxNumber) throws InterruptedException {
        ExecutorService executorService = Executors.newWorkStealingPool();
        List<Callable<Integer>> callableList = Arrays.asList(getSum(maxNumber));
        executorService.invokeAll(callableList).stream().map(f->{
                    try{
                        return f.get();
                    }     catch (Exception e) {
                        logger.info(GENERAL_EXCEPTION);
                        throw new IllegalStateException(e);
                    }
                }).forEach(System.out::println);
    }
    public static void run() {
        int userTyping = readInfoFromUser();
        try {
            new FibonacciNumber().printSumOfFibonacciNumber(userTyping);
        } catch (InterruptedException e) {
            logger.error(INTERRUPTED_EXCEPTION);
        }
    }
    public static int readInfoFromUser() {
        logger.info(ENTER_NUMBER_INFO);
        Scanner scanner = new Scanner(System.in);
        int userTyping = scanner.nextInt();
        return userTyping;
    }
    }

